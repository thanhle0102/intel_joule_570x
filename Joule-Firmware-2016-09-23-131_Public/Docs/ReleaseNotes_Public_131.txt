============================================================
BIOS Version #131 - 2016-09-23
------------------------------------------------------------
 1. Fixed corner case in MRC where DVT2.1 4GB SKU was hanging.
------------------------------------------------------------

============================================================
BIOS Version #130 - 2016-09-23
------------------------------------------------------------
  
  1. Fixed PPV Debug version hang.
  2. Fixed GRUB hanging issue.
  3. Fixed BDS finding white listed boot loaders.
  4. Fixed CMOS initialization code for battery-less systems.
  5. Sync to the latest EEPROM code.
     - [455] Some LEDs are left on.
     - Only sync internal copy when the version rev is higher.
  
  6. [323]
     - Unused VPROG rails are getting turned on.
       - Disable all VPROG rails by default.
  7. [432]
     - Need to program the correct VPROG rails for the MIPI
       cameras.
       - Need to go into BIOS Setup -> System Configuration
         -> VPROG Rails to enable them.
  8. [460]
     - We now have mixed geometry 3GB parts. This was causing
       the line to fail 100% of the 25nm 3GB parts. Created a
       way to support all 4 of the different Micron memory SKUs
       with one BIOS image.
       -   +------------------------------ Memory size
           |     +------------------------ Technology
           |     |      +----------------- RankEnable
           |     |      |   +------------- DramDensity
           |     |      |   |   +--------- ID1
           |     |      |   |   |   +----- ID2
           |     |      |   |   |   |   +- Periodic Training Enabled
         +-----+------+---+---+---+---+-----+
         | 3GB | 20nm | 1 | 3 | 1 | 0 | No  |
         | 3GB | 25nm | 3 | 1 | 0 | 1 | Yes |
         | 4GB | 20nm | 3 | 2 | 2 | 0 | No  |
         | 4GB | 25nm | 3 | 2 | 1 | 2 | Yes |
         +-----+------+---+---+---+---+-----+
  9. [459]
     - Remove PMIC register 0x4FB6 & 0x4FB7 from the SMIP
       blacklist.
 10. [471]
     - BDS doesn't enforce EFI System Partition rules.
       - Fixed by testing the handle for ESP and removable.
         If BSP is true, then entire whitelist valid.
         If Removable, then only \EFI\Boot\Bootx64.efi valid.

============================================================
